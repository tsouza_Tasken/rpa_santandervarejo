const path = require('path');

module.exports = {
   entry: {
    script: path.join(__dirname, 'src', 'app.js'),
    "src/www/cacs-web/app": path.join(__dirname, 'src/www/cacs-web', 'app.js'),
    "src/www/portalSantander-web/app": path.join(__dirname, 'src/www/portalSantander-web', 'app.js')
   },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [{
      test: /\.js$/i,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: [
          ['@babel/preset-env', {
            modules: false,
            targets: {
              explorer: '6',
            },
            useBuiltIns: 'usage',
          }],
        ],
      },
    }],
  },
  mode: process.env.NODE_ENV || 'production',
};
