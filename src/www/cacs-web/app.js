const cacsData = {};

Object.assign(cacsData, {
  activationData: {
    type : '',
    contact: ''
  },
});

async function waitForElement(selector, frame = '') {
  return new Promise((resolve, reject) => {
    const time = window.rpa.FinanceDelay;
    const nameFrame = frame;
    let k = 0;
    const interval = setInterval(() => {
      let el = null;
      if (frame !== '') {
        el = window.frames[`${nameFrame}`].document.querySelector(selector);
      } else {
        el = document.querySelector(selector);
      }
      if (el) {
        clearInterval(interval);
        resolve(el);
        return;
      }
      k += 1;
      if (k === parseInt(window.rpa.MaxWait / time, 10)) {
        clearInterval(interval);
        reject(new Error(`Erro ao esperar pelo elemento ${selector}`));
      }
    }, time);
  });
}

async function getElement(selector, frame) {
  try {
    const el = await waitForElement(selector, frame);
    return el;
  } catch (error) {
    throw new Error(error);
  }
}

async function type(selector, text, frame) {
  const el = await getElement(selector, frame);
  if (el) {
    el.value = text;
  }
}

async function click(selector, frame) {
  const el = await getElement(selector, frame);
  if (el) {
    const event = document.createEvent('MouseEvent');
    event.initEvent('click', true, true);
    el.dispatchEvent(event);
  }
}

async function check(selector) {
  const el = await getElement(selector);
  if (el) {
    el.checked = true;
  }
}

function clickAsyncArray(elements, frame, time = window.rpa.FinanceDelay) {
  const i = 90;
  let k = 0;
  const found = [];
  let look = elements;
  const nameFrame = frame;
  const interval = setInterval(() => {
    k += 1;
    if (window.rpa.MaxWait / time === k || !look.length) {
      clearInterval(interval);
    }
    look.forEach((value) => {
      let el = null;
      if (nameFrame) {
        el = window.frames[`${nameFrame}`].document.querySelector(value);
      } else {
        el = document.querySelector(value);
      }
      if (el) {
        found.push(value);
        const event = document.createEvent('MouseEvent');
        event.initEvent('click', true, true);
        el.dispatchEvent(event);
      }
    });
    look = look.filter((e) => found.indexOf(e) >= 0 ? false : true);
  }, time);
}

async function verificarLogin(){
  var n = window.frames[0].document.querySelector('p[class="HeadingTextRed"]');
  return n == null
}

async function setActiveActivationData(actionPlace) {
  cacsData.activationData.type = 'OC';
  document.querySelector('[name=NewAct_PlaceCalled]').value = actionPlace;
}

async function setReceptiveActivationData() {
  cacsData.activationData.type = 'IC';
}

async function setTelephoneFields(numero, tipo) {
  // tipo
  // B-Comercial
  // R-Residencial
  // P-Celular
  //formato
  //00 - internacional
  //01 - Norte Americano
  //<select  name="Dem_PhAvail1" class="InputFields"><option value="">.....</option><option value="A">Apos Horario Comercial</option><option value="B">Horario Comercial</option><option value="L">Nao Listado</option><option value="N">Nao Ligar para o Numero</option><option value="P">Nao Publicado</option><option value="T">Telefone de Terceiros</option><option selected="true" value="V">Valido</option><option value="X">Numero Invalido</option></select>

  document.querySelector('[name="Dem_PhType1"]').value = tipo;
  document.querySelector('[name="Dem_PhFmt1"]').value = "00";
  document.querySelector('[name="Dem_Ph1"]').value = numero;
  document.querySelector('[name="Dem_PhAvail1"]').value = "V"; 
  document.querySelector('[name="Dem_Prefix"]').value = "";  
  document.querySelector('[name="Dem_Suffix"]').value = "";  
  document.querySelector('[name="Dem_NameRel"]').value = "";  
  document.querySelector('[name="Dem_DemStat"]').value = "";  
  document.querySelector('[name="Dem_RightToPrivacy"]').value = "";  
  document.querySelector('[name="Dem_TaxIdType"]').value = "";  
  document.querySelector('[name="Dem_AddrRel"]').value = "";  
  document.querySelector('[name="Dem_StProv"]').value = "";  
  document.querySelector('[name="Dem_CountryCd"]').value = "";  
  document.querySelector('[name="Dem_Lang"]').value = "";  
  document.querySelector('[name="Dem_PhFmt2"]').value = "";  
  document.querySelector('[name="Dem_PhType2"]').value = "";  
  document.querySelector('[name="Dem_PhAvail2"]').value = "";  
  document.querySelector('[name="Dem_PhFmt3"]').value = "";  
  document.querySelector('[name="Dem_PhType3"]').value = "";  
  document.querySelector('[name="Dem_PhAvail3"]').value = "";
  document.querySelector('[name="Dem_UDefDrop1"]').value = "";
  document.querySelector('[name="Dem_UDefDrop2"]').value = "";
  document.querySelector('[name="Dem_UDefDrop3"]').value = "";
  document.querySelector('[name="Dem_UDefDrop4"]').value = "";  
}

async function setDealActivationData(actionText) {
  cacsData.activationData.type = 'RF';
  document.querySelector('[name=NewAct_CollAct]').value = cacsData.activationData.type;
  document.querySelector('[name=NewAct_SubmitButton]').value = 'Y';
  document.querySelector('[name=NewAct_SumitAndNextButton]').value = 'N';
  document.querySelector('[name=reloadProm]').value = 'N';
  document.querySelector('[name=NewAct_HistText]').value = actionText;
  document.querySelector('[name=NewAct_ContCd]').value = "";
  document.querySelector('[name=NewAct_Excuse]').value = "";
  document.querySelector('[name=NewAct_PromAmt1]').value = "";
  document.querySelector('[name=NewAct_PromDate1]').value = "";
}

async function setCommonActivationData(actionText, actionContact, actionReason, deal, dealValue, dealDate) {
  document.querySelector('[name=NewAct_CollAct]').value = cacsData.activationData.type;
  document.querySelector('[name=NewAct_HistText]').value = actionText;
  document.querySelector('[name=NewAct_ContCd]').value = actionContact !== "" && actionContact ? actionContact : 'A';
  document.querySelector('[name=NewAct_SubmitButton]').value = 'Y';
  document.querySelector('[name=NewAct_SumitAndNextButton]').value = 'N';
  document.querySelector('[name=reloadProm]').value = 'N';
  document.querySelector('[name=NewAct_Excuse]').value = actionReason;
  if (deal === true){
    document.querySelector('[name=NewAct_PromAmt1]').value = dealValue;
    document.querySelector('[name=NewAct_PromDate1]').value = dealDate;
  }
}

function submitForm(step) {
  parent.step = step;
  document.forms[0].submit();
}

function reportActivationError() {
  try {
      parent.step == 'ztwAccess';
      parent.appData.cacsData.Status = 'internal error';
      external.StoreData('cacsActivationStatus', JSON.stringify(parent.appData.cacsData));
      var link = $('a:contains("rea de Trabalho")');
      link[0].click();
      throw "Erro ao acionar ztwAccess";
  } catch(e) {
      throw "Erro ao acionar ztwAccess";
  }
}

async function simulateFormSubmit(step, formData) {
  $.ajax({
      type: 'POST',
      url: 'https://caweb.santander.com.br/CACSwebNegocios/AmsServlet.jsp',
      //'https://cacswebnegocios.sb/CACSwebCorp/AmsServlet.jsp',
      data: formData,
      dataType: 'text',
      headers: {
          'Accept': 'image/gif, image/jpeg, image/pjpeg, application/x-ms-application, application/xaml+xml, application/x-ms-xbap, */*',
          'Accept-Encoding': 'gzip, deflate',
          'Accept-Language': 'pt-BR, pt; q=0.5',
          'Connection': 'Keep-Alive',
          'Content-Type': 'application/x-www-form-urlencoded',
          'User-Agent': navigator.userAgent
      },
      success: function(data) {
          if (data.indexOf('function showErrors') == -1) {
            submitForm(step);
          } else {
            reportActivationError();
          }
      },
      error: function(err) {
        reportActivationError();
      }
  });
}

async function verifyINA(){
  return document.querySelector("p[class='whiteHeaderText rightMargin']").textContent.indexOf('INA') > 0;
}

async function goToCacsWeb() {
  document.location.href = "http://portalcorporativo.sb/ZTW_CACS_WEB/ASP/ZTW_GERATICKET_REDIRECT.ASP";
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

window.rpa = {
  clickAsyncArray,
  check,
  click,
  type,
  getElement,
  goToCacsWeb,
  waitForElement,
  cacsWeb: async () => {
    return new Promise(async (resolve, reject) => {
      try {
        await waitForElement("frame[name='mainFrame']");
        await window.rpa.waitForElement("form[name='StartPortfolioForm']", 'mainFrame'); 
        resolve({ sucess : true });
      } catch (e) {
        reject({ sucess : false, message: `Ocorreu um erro ao logar: ${e}`});
      }
   });
  },
  getResumo: async () => {
    return new Promise(async (resolve, reject) => {
      try {
        var el = await waitForElement("div[id='dijit_layout_ContentPane_0'] table tbody tr td pre");
        resolve(el.innerText);
      } catch (e) {
        reject({ sucess : false, message: `Ocorreu um erro ao obter o detalhamento da divida: ${e}`});
      }
   });
  },
  getDados: async () => {
    return new Promise(async (resolve, reject) => {
      try {
        var el = await waitForElement("div[id='dijit_layout_ContentPane_1'] table tbody tr td pre");
        var dados = "";
        if (el){
          var els = document.querySelectorAll("div[id='dijit_layout_ContentPane_1'] table tbody tr td pre");
          for(var i = 0, len = els.length; i < len; ++i) {
            dados = dados.concat(els[i].innerText) + '\n';
          }
        }
        resolve(dados);
      } catch (e) {
        reject({ sucess : false, message: `Ocorreu um erro ao obter os dados da divida: ${e}`});
      }
   });
  },
  addTelephone: async (numero, tipo) => {
    return new Promise(async (resolve, reject) => {
      try {
        await setTelephoneFields(numero, tipo);
        await simulateFormSubmit("saveActivation", {
          Dem_PhType1 : document.forms[0].Dem_PhType1.value,
          Dem_PhAvail1 : document.forms[0].Dem_PhAvail1.value,
          Dem_Ph1 : document.forms[0].Dem_Ph1.value,
          Dem_PhFmt1 : document.forms[0].Dem_PhFmt1.value,
          Dem_SaveButton2 : 'OK',
          Dem_ClearButton2 : 'Limpar',
          Dem_CancelButton2 : 'Cancelar',
          Dem_Prefix : document.querySelector('[name="Dem_Prefix"]').value,  
          Dem_Suffix : document.querySelector('[name="Dem_Suffix"]').value,
          Dem_NameRel : document.querySelector('[name="Dem_NameRel"]').value, 
          Dem_DemStat : document.querySelector('[name="Dem_DemStat"]').value,  
          Dem_RightToPrivacy : document.querySelector('[name="Dem_RightToPrivacy"]').value, 
          Dem_TaxIdType : document.querySelector('[name="Dem_TaxIdType"]').value, 
          Dem_AddrRel : document.querySelector('[name="Dem_AddrRel"]').value,  
          Dem_StProv : document.querySelector('[name="Dem_StProv"]').value,  
          Dem_CountryCd : document.querySelector('[name="Dem_CountryCd"]').value,  
          Dem_Lang : document.querySelector('[name="Dem_Lang"]').value,
          Dem_PhFmt2 : document.querySelector('[name="Dem_PhFmt2"]').value,  
          Dem_PhType2 : document.querySelector('[name="Dem_PhType2"]').value,  
          Dem_PhAvail2 : document.querySelector('[name="Dem_PhAvail2"]').value,  
          Dem_PhFmt3 : document.querySelector('[name="Dem_PhFmt3"]').value,  
          Dem_PhType3 : document.querySelector('[name="Dem_PhType3"]').value,  
          Dem_PhAvail3 : document.querySelector('[name="Dem_PhAvail3"]').value,  
          Dem_UDefDrop1 : document.querySelector('[name="Dem_UDefDrop1"]').value,
          Dem_UDefDrop2 : document.querySelector('[name="Dem_UDefDrop2"]').value,
          Dem_UDefDrop3 : document.querySelector('[name="Dem_UDefDrop3"]').value,
          Dem_UDefDrop4 : document.querySelector('[name="Dem_UDefDrop4"]').value,  
          AcntTimer: document.forms[0].AcntTimer.value,
          Close_Button: 'Fe%3CU%3Ec%3C%2FU%3Ehar',
          FP_Button: 'Registro+R%E1pido',
          nextTab: document.forms[0].nextTab.value,
          resetTab: document.forms[0].resetTab.value,
          ni: document.forms[0].ni.value,
          demId: document.forms[0].demId.value,
          mgtSumId: document.forms[0].mgtSumId.value,
          navMgtViews: document.forms[0].navMgtViews.value,
          readOnly : false,
          Add : true,
          isLegal : false,
          fromSummaryLink : false,
          AcctType : null,
          isHidden : true,
        });
        resolve({ sucess : true });
      } catch (e) {
        reject({ sucess : false, message: `Ocorreu um erro ao inserir novo telefone: ${e}`});
      }
   });
  },
  cacsWebInsertDealAction: async (actionText) => {
    return new Promise(async (resolve, reject) => {
      try {
        await setDealActivationData(actionText);
        await simulateFormSubmit("saveActivation", {
            AcntTimer: document.forms[0].AcntTimer.value,
            Close_Button: 'Fe%3CU%3Ec%3C%2FU%3Ehar',
            demId: document.forms[0].demId.value,
            FP_Button: 'Registro+R%E1pido',
            Hst_ExpandedRows: document.forms[0].Hst_ExpandedRows.value,
            Hst_MaxItemCnt: document.forms[0].Hst_MaxItemCnt.value,
            mgtSumId: document.forms[0].mgtSumId.value,
            navMgtViews: document.forms[0].navMgtViews.value,
            NewAct_CollAct: document.forms[0].NewAct_CollAct.value,
            NewAct_HistText: document.forms[0].NewAct_HistText.value,
            NewAct_SubmitButton: document.forms[0].NewAct_SubmitButton.value,
            NewAct_SumitAndNextButton: document.forms[0].NewAct_SumitAndNextButton.value,
            nextTab: document.forms[0].nextTab.value,
            ni: document.forms[0].ni.value,
            prevAcctNum: document.forms[0].prevAcctNum.value,
            prevLocCd: document.forms[0].prevLocCd.value,
            reloadProm: document.forms[0].reloadProm.value,
            resetTab: document.forms[0].resetTab.value,
        });
        resolve({ sucess : true });
      } catch (e) {
        reject({ sucess : false, message: `Ocorreu um erro ao acionar acordo: ${e}`});
      }
    })
  },
  cacsWebInsertAction: async (actionType, actionText, actionPlace, actionContact, actionReason, deal, dealValue, dealDate) => {
    return new Promise(async (resolve, reject) => {
      try {
        var INA = await verifyINA();
        if (INA === true){
          reject({ sucess : false, message: `Contrato INA`});
          return;
        }

        if (actionType === 'ativo'){
          await setActiveActivationData(actionPlace);
        } else {
          await setReceptiveActivationData();
        } 
        await setCommonActivationData(actionText, actionContact, actionReason, deal, dealValue, dealDate);
        await simulateFormSubmit("saveActivation", {
            AcntTimer: document.forms[0].AcntTimer.value,
            Close_Button: 'Fe%3CU%3Ec%3C%2FU%3Ehar',
            demId: document.forms[0].demId.value,
            FP_Button: 'Registro+R%E1pido',
            Hst_ExpandedRows: document.forms[0].Hst_ExpandedRows.value,
            Hst_MaxItemCnt: document.forms[0].Hst_MaxItemCnt.value,
            mgtSumId: document.forms[0].mgtSumId.value,
            navMgtViews: document.forms[0].navMgtViews.value,
            NewAct_CollAct: document.forms[0].NewAct_CollAct.value,
            NewAct_ContCd: document.forms[0].NewAct_ContCd.value,
            NewAct_HistText: document.forms[0].NewAct_HistText.value,
            NewAct_PlaceCalled: document.forms[0].NewAct_PlaceCalled.value,
            NewAct_SubmitButton: document.forms[0].NewAct_SubmitButton.value,
            NewAct_SumitAndNextButton: document.forms[0].NewAct_SumitAndNextButton.value,
            nextTab: document.forms[0].nextTab.value,
            ni: document.forms[0].ni.value,
            prevAcctNum: document.forms[0].prevAcctNum.value,
            prevLocCd: document.forms[0].prevLocCd.value,
            reloadProm: document.forms[0].reloadProm.value,
            resetTab: document.forms[0].resetTab.value,
            NewAct_PromAmt1: document.forms[0].NewAct_PromAmt1.value,
            NewAct_PromDate1: document.forms[0].NewAct_PromDate1.value,
            NewAct_Excuse: document.forms[0].NewAct_Excuse.value,
        });
        resolve({ sucess : true });
      } catch (e) {
        reject({ sucess : false, message: `Ocorreu um erro ao acionar: ${e}`});
      }
   });
  },
  FinanceDelay: 1000,
  MaxWait: 3000,
};