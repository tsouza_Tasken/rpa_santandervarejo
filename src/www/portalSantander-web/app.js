//const sites = require('../../config/config');

const referenceAttributes = {};

Object.assign(referenceAttributes, {
  sideLeftMenu: {
    frame: {
      name: 'menu',
      selector: 'frame[name="menu"]',
    },
    linkCacsWeb: 'td[id="nivel06"]',
  },
  login: {
    frame: {
      name: 'principal',
      selector: 'frame[name="principal"]',
    },
    nameUser: 'input[name="txtUsuario"]',
    passwordUser: 'input[name="pswSenha"]',
    btnConfirm: 'a[href="javascript:onEnviar();"]',
  },
});

async function waitForElement(selector, frame = '', time = window.rpa.FinanceDelay) {
  return new Promise((resolve, reject) => {
    //const time = window.rpa.FinanceDelay;
    const nameFrame = frame;
    let k = 0;
    const interval = setInterval(() => {
      let el = null;
      if (frame) {
        el = window.frames[`${nameFrame}`].document.querySelector(selector);
      } else {
        el = document.querySelector(selector);
      }
      if (el) {
        clearInterval(interval);
        resolve(el);
        return;
      }
      k += 1;
      if (k === parseInt(window.rpa.MaxWait / time, 10)) {
        clearInterval(interval);
        reject(new Error(`Erro ao esperar pelo elemento ${selector}`));
      }
    }, time);
  });
}

async function waitForElementClear(selector, frame = '', time = window.rpa.FinanceDelay) {
  return new Promise((resolve, reject) => {
    //const time = window.rpa.FinanceDelay;
    const nameFrame = frame;
    let k = 0;
    const interval = setInterval(() => {
      let el = null;
      if (frame) {
        el = window.frames[`${nameFrame}`].document.querySelector(selector);
      } else {
        el = document.querySelector(selector);
      }
      if (!el) {
        clearInterval(interval);
        resolve(el);
        return;
      }
      k += 1;
      if (k === parseInt(window.rpa.MaxWait / time, 10)) {
        clearInterval(interval);
        reject(new Error(`Erro ao esperar pelo elemento ${selector}`));
      }
    }, time);
  });
}

async function getElement(selector, frame) {
  try {
    const el = await waitForElement(selector, frame);
    return el;
  } catch (error) {
    throw new Error(error);
  }
}

async function type(selector, text, frame) {
  const el = await getElement(selector, frame);
  if (el) {
    el.value = text;
  }
}

async function click(selector, frame) {
  const el = await getElement(selector, frame);
  if (el) {
    const event = document.createEvent('MouseEvent');
    event.initEvent('click', true, true);
    el.dispatchEvent(event);
  }
}

async function check(selector) {
  const el = await getElement(selector);
  if (el) {
    el.checked = true;
  }
}

function clickAsyncArray(elements, frame, time = window.rpa.FinanceDelay) {
  const i = 90;
  let k = 0;
  const found = [];
  let look = elements;
  const nameFrame = frame;
  const interval = setInterval(() => {
    k += 1;
    if (window.rpa.MaxWait / time === k || !look.length) {
      clearInterval(interval);
    }
    look.forEach((value) => {
      let el = null;
      if (nameFrame) {
        el = window.frames[`${nameFrame}`].document.querySelector(value);
      } else {
        el = document.querySelector(value);
      }
      if (el) {
        found.push(value);
        const event = document.createEvent('MouseEvent');
        event.initEvent('click', true, true);
        el.dispatchEvent(event);
      }
    });
    look = look.filter((e) => found.indexOf(e) >= 0 ? false : true);
  }, time);
}

async function login(acc, password) {
  await type(referenceAttributes.login.nameUser, acc, referenceAttributes.login.frame.name);
  await type(referenceAttributes.login.passwordUser, password, referenceAttributes.login.frame.name);
  clickAsyncArray([`${referenceAttributes.login.btnConfirm}`], referenceAttributes.login.frame.name);
}

async function goToCacsWeb() {
  // document.location.href = "http://portalcorporativo.sb/ZTW_CACS_WEB/ASP/ZTW_GERATICKET_REDIRECT.ASP";
  $.ajax({
    type: 'GET',
    url: "https://escobs.santandernegocios.com.br/ZTW_CACS_WEB/ASP/ZTW_GERATICKET.ASP",
    dataType: 'text',
    headers: {
      'Accept': 'text/html, application/xhtml+xml, image/jxr, */*',
      'Accept-Encoding': 'gzip, deflate',
      'Accept-Language': 'pt-BR, pt; q=0.5',
      'Connection': 'Keep-Alive',
      'Cookie': document.cookie
    },
    success: function(data) {
        window.frames['cliente'].document.body.innerHTML = "";
        window.frames['cliente'].document.body.innerHTML = data;
        accessZtwPortal();
    },
    error: function(err) {
        
    }
  });
}

function accessZtwPortal(){
  window.frames['cliente'].document.frmTicket.target = '_top';
  window.frames['cliente'].document.frmTicket.action = 'https://caweb.santander.com.br/CACSwebNegocios/GL_FrameSet.jsp';
  window.frames['cliente'].document.frmTicket.submit();

  //form1
}

window.rpa = {
  clickAsyncArray,
  check,
  click,
  type,
  getElement,
  waitForElement,
  goToCacsWeb,
  accessZtwPortal,
  loginCacs: async () => {
    await goToCacsWeb();
    return true;
  },
  portalSantander: async (loginEv, user, password) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (loginEv === true){
          await login(user, password);
          await waitForElementClear(referenceAttributes.login.nameUser, referenceAttributes.login.frame.name);
        }
        await waitForElement("frame[name='principal']");
        resolve(true);
      } catch (e) {
        reject(false);
      }
   });
  },
  FinanceDelay: 50,
  MaxWait: 3000,
};