const Nightmare = require('nightmare');
require('nightmare-iframe-manager')(Nightmare);
const path = require('path');
const fs = require('fs');
var js2xmlparser = require("js2xmlparser");
var fs2 = require('fs');


const url = 'https://frontunicostg.paas.santanderbr.corp/#/agencia';
 const showInstance = true;

const nightmareOptions = {
  show: showInstance,
  dock: showInstance,
  alwaysOnTop: showInstance,
  openDevTools: { show: showInstance },
  executionTimeout: showInstance ? 100000 : 10000,
  gotoTimeout: showInstance ? 30000 : 5000,
  switches: {
    'ignore-certificate-errors': true
  }
};

function escreverLog(texto){
  var date = new Date();
  var current_hour = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
  var date_str = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
  caminho = path.join(process.cwd(), "/Log.txt");
  if (!fs.existsSync(caminho)){
    var stream = fs.createWriteStream(caminho);
      stream.once('open', function(fd) {
      stream.write(`${date_str} ${current_hour} - ${texto}`);
      stream.end();
    });
  }else{
    fs.appendFileSync(caminho, `\n${date_str} ${current_hour} - ${texto}`);
  }
}

var nightmare = Nightmare(nightmareOptions);

module.exports = {
    Login: async args => new Promise(async (res, rej) => {
    try {
      await nightmare.goto(url);

      await nightmare.inject('js', path.join(process.cwd(), 'dist/script.js'));
      const ret = await nightmare.evaluate(() => { return window.rpa["ReturnCaptchaLogin"]().then(i => i).catch(e => e); }, );

      if ((ret.imgName !== "") && (ret.imgName)){
        caminho = path.join(process.cwd(), "/Log.png");
        var stream2 = fs2.createWriteStream(caminho);
        stream2.once('open', function(fd) {
          fs2.writeFileSync(ret.imgName, ret.xhr, "binary");
        stream2.end();
        });
        res(ret.imgName);
      } else {
        rej(new Error(`Error: ${ret}`));
      }
    }catch (e) {
      const messageError = typeof e === 'string' ? e : (typeof e.message === 'string' ? e.message : '');
      const shouldIncludeRepeat = (messageError.includes('#TRYAGAIN#') || messageError.includes('navigation error'))
      rej(new Error(`${e}${ shouldIncludeRepeat ? '#TRYAGAIN#' : ''}`));
    } 
  })
};