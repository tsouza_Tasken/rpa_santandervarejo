const Nightmare = require('nightmare');
require('nightmare-iframe-manager')(Nightmare);
const path = require('path');
const sites = require("../../config/configEscobs")

 const showInstance = true;

const nightmareOptions = {
  show: true,
  dock: showInstance,
  alwaysOnTop: showInstance,
  openDevTools: { show: showInstance },
  executionTimeout: showInstance ? 100000 : 10000,
  gotoTimeout: showInstance ? 30000 : 5000,
  switches: {
    'ignore-certificate-errors': true
  }
};

async function goToPortalSantander(login, user, password){
  return new Promise(async (res, rej) => {
    await nightmare.inject('js', path.join(process.cwd(), "/src/www/jquery/jquery-3.3.1.js"));
    await nightmare.inject('js', path.join(process.cwd(), '/src/www/portalSantander-web/app.js'));

    var ret = await nightmare.evaluate(
      (loginEv, userEv, passwordEv) => {
        return window.rpa.portalSantander(loginEv, userEv, passwordEv)
        .then(i => i)
        .catch(e => e);
      },
      login, user, password,
    );

    if (!ret) {
      await nightmare.end();
      rej(`Error: Erro ao logar #TRYAGAIN#`);
    }

    res("Logado");
  });
}

async function goToCacsWeb(nightmare, cpf, type, login, user, password)
{
  return new Promise(async (res, rej) => {
    try{
      var ret = await goToPortalSantander(login, user, password);
      if (ret !== "Logado")
        rej("Erro ao logar no portal Santander");

      await nightmare.evaluate(
        () => {
          return window.rpa.loginCacs()
          .then(i => i)
          .catch(e => e);
        }
      );
      
      await nightmare.wait(sites.cacsWeb.referenceAttributes.cacs.cacsWebRedirectPage);

      await nightmare.inject('js', path.join(process.cwd(), '/src/www/cacs-web/app.js'));
      ret = null;
      ret = await nightmare.evaluate(
        () => { 
          return window.rpa.cacsWeb()
          .then(i => i)
          .catch(e => e);
        },
      );
      
      if (ret != null && ret.sucess) {
        await nightmare.goto(sites.cacsWeb.getUrlContract(cpf, type)).wait(sites.cacsWeb.referenceAttributes.cacs.cacsWebRedirectClient);
      }else{
        await nightmare.end();
        rej(`Error:${ret.message} #TRYAGAIN#`);
      }

      res('Sucesso');
    }catch(e)
    {
      const messageError = typeof e === 'string' ? e : (typeof e.message === 'string' ? e.message : '');
      rej(new Error(messageError));
    }
  })
}

module.exports = {
  AdicionarTelefone: async args => new Promise(async (res, rej) => {
    const { cpf, type, numero, tipoTelefone, login, user, password } = args;
    process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;
    const nightmare = Nightmare(nightmareOptions);

    try
    {
      await nightmare.goto(`${sites.cacsWeb.url}`);
      await goToCacsWeb(nightmare, cpf, type, login, user, password);

      ret = await nightmare
      .evaluate(() => {
        var headings = document.evaluate("//a[text()='Contatos']", document, null, XPathResult.ANY_TYPE, null );
        var thisHeading = headings.iterateNext();
        if (thisHeading)
          thisHeading.click();
        else  
          new Error("Erro ao abrir Contatos");
      })
      .wait("input[name='Add']")
      .click("input[name='Add']")
      .inject('js', path.join(process.cwd(), "/src/www/jquery/jquery-3.3.1.js"))
      .inject('js', path.join(process.cwd(), '/src/www/cacs-web/app.js'))
      .evaluate((numeroEv, tipoTelefoneEv) => { return window.rpa.addTelephone(numeroEv, tipoTelefoneEv) }, numero, tipoTelefone);
      //.end();

      if (ret !== "")
        res(ret);
      else
        res(new Error('Erro ao obter dados dos contratos'));
    }
    catch(e)
    {
      await nightmare.end();
      const messageError = typeof e === 'string' ? e : (typeof e.message === 'string' ? e.message : '');
      rej(new Error(messageError));
    }
  }),
  ObterDadosContrato: async args => new Promise(async (res, rej) => {
    const { cpf, type, login, user, password } = args;
    process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;
    const nightmare = Nightmare(nightmareOptions);

    try
    {
      await nightmare.goto(`${sites.cacsWeb.url}`);
      await goToCacsWeb(nightmare, cpf, type, login, user, password);

      ret = await nightmare
      .inject('js', path.join(process.cwd(), 'src/www/cacs-web/app.js'))
      .evaluate(() => { return window.rpa.getDados() })
      .end();

      if (ret !== "")
        res(ret);
      else
        res(new Error('Erro ao obter dados dos contratos'));
    }
    catch(e)
    {
      await nightmare.end();
      const messageError = typeof e === 'string' ? e : (typeof e.message === 'string' ? e.message : '');
      rej(new Error(messageError));
    }
  }),
  ObterResumoContrato: async args => new Promise(async (res, rej) => {
    const { cpf, type, login, user, password } = args;
    process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;
    const nightmare = Nightmare(nightmareOptions);

    try
    {
      await nightmare.goto(`${sites.cacsWeb.url}`);
      await goToCacsWeb(nightmare, cpf, type, login, user, password);

      ret = await nightmare
      .inject('js', path.join(process.cwd(), '/src/www/cacs-web/app.js'))
      .evaluate(() => { return window.rpa.getResumo() })
      .end();

      if (ret !== "")
        res(ret);
      else
        res(new Error('Erro ao obter resumo do contrato'));
    }
    catch(e)
    {
      await nightmare.end();
      const messageError = typeof e === 'string' ? e : (typeof e.message === 'string' ? e.message : '');
      rej(new Error(messageError));
    }
  }),
  AcionarCacsWeb: async args => new Promise(async (res, rej) => {
    const { contract, type, actionType, actionText, actionPlace, actionContact, actionReason, login,
            user, password, deal, dealValue, dealDate } = args;
    process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;
    const nightmare = Nightmare(nightmareOptions);
    try {
      // escreverLog("Antes goto " + sites.cacsWeb.url);
      await nightmare.goto(`${sites.cacsWeb.url}`);
      // escreverLog("Após primeiro goto");
      await nightmare.inject('js', path.join(process.cwd(), "/src/www/jquery/jquery-3.3.1.js"));
      await nightmare.inject('js', path.join(process.cwd(), '/src/www/portalSantander-web/app.js'));
      // escreverLog("Após inject santander");
      var ret = await nightmare.evaluate(
        (loginEv, userEv, passwordEv) => {
          return window.rpa.portalSantander(loginEv, userEv, passwordEv)
          .then(i => i)
          .catch(e => e);
        },
        login, user, password,
      );

      if (!ret) {
        await nightmare.end();
        rej(`Error: Erro ao logar #TRYAGAIN#`);
        return;
      }

      await nightmare.evaluate(
        () => {
          return window.rpa.loginCacs()
          .then(i => i)
          .catch(e => e);
        }
      );
      await nightmare.wait(sites.cacsWeb.referenceAttributes.cacs.cacsWebRedirectPage);
      
      //injetando js novamente pois a pagina atualizou
      await nightmare.inject('js', path.join(process.cwd(), '/src/www/cacs-web/app.js'));
      ret = null;
      ret = await nightmare.evaluate(
        () => { 
          return window.rpa.cacsWeb()
          .then(i => i)
          .catch(e => e);
        },
      );
      
      if (ret != null && ret.sucess) {
        await nightmare.goto(sites.cacsWeb.getUrlContract(contract, type)).wait(sites.cacsWeb.referenceAttributes.cacs.cacsWebRedirectClient);
      }else{
        await nightmare.end();
        rej(`Error:${ret.message} #TRYAGAIN#`);
        return;
      }
      
      //inserindo novamente pois a pagina atualizou
      await nightmare.inject('js', path.join(process.cwd(), "/src/www/jquery/jquery-3.3.1.js"));
      await nightmare.inject('js', path.join(process.cwd(), '/src/www/cacs-web/app.js'));
      ret = null;
      ret = await nightmare.evaluate(
        (actionTypeEv, actionTextEv, actionPlaceEv, actionContactEv, actionReasonEv, dealEv, dealValueEv, dealDateEv) => { 
          return window.rpa.cacsWebInsertAction(actionTypeEv, actionTextEv, actionPlaceEv, actionContactEv, actionReasonEv, dealEv, dealValueEv, dealDateEv)
          .then(i => i)
          .catch(e => e);
        },
        actionType, actionText, actionPlace, actionContact, actionReason, deal, dealValue, dealDate
      );

      // escreverLog('Verificando se houve acordo');
      var ret2 = null;
      if (deal === true && ret != null && ret.sucess) {
        // escreverLog('Inserindo acionamento acordo');
        try{
          await nightmare.goto(sites.cacsWeb.getUrlContract(contract, type)).wait(sites.cacsWeb.referenceAttributes.cacs.cacsWebRedirectClient);
          await nightmare.inject('js', path.join(process.cwd(), "/src/www/jquery/jquery-3.3.1.js"));
          await nightmare.inject('js', path.join(process.cwd(), '/src/www/cacs-web/app.js'));
          ret2 = await nightmare.evaluate(
            (actionTextEv) => { 
              return window.rpa.cacsWebInsertDealAction(actionTextEv)
              .then(i => i)
              .catch(e => e);
            },
            actionText
          );
        }
        catch(e){
          
        }
      }

      await nightmare.end();
      if ((ret != null && ret.sucess) && ((ret2 != null && ret2.sucess) || (deal === false))) {
        res("Sucesso");
      } else {
        if (ret == null){
          rej(`Error:Não houve retorno do acionamento #TRYAGAIN#`);
          return;
        }
        if (ret.sucess === false){
          rej(`Error:${ret.message} #TRYAGAIN#`);
          return;
        }
        if (ret2 == null){
          rej(`Error:Não houve retorno do acionamento de acordo`);
          return;
        }
        if (ret2.sucess === false){
          rej(`Error:${ret2.message}`);
          return;
        }
      }
    } catch (e) {
      await nightmare.end();
      const messageError = typeof e === 'string' ? e : (typeof e.message === 'string' ? e.message : '');
      const shouldIncludeRepeat = (messageError.includes('#TRYAGAIN#') || messageError.includes('navigation error'))
      rej(new Error(`Error geral:${e}${ shouldIncludeRepeat ? '#TRYAGAIN#' : ''}`));
    }
  }),
};