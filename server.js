const express = require('express');
const basicAuth = require('express-basic-auth')
const graphqlHTTP = require('express-graphql');
const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
  GraphQLBoolean,
} = require('graphql');

const controllerEscobs = require('./src/controller/escobs');
const controllerC2 = require('./src/controller/c2');
const nodeEnv = process.env.NODE_ENV || 'production';

const query_c2 = new GraphQLObjectType({
  name: 'query',
  fields: {
    Login: {
      type: GraphQLString,
      args: {
        fix: { type: GraphQLString },
      },
      resolve: (_, args) => controllerC2.Login(args),
    }
  }
});

const query_escobs = new GraphQLObjectType({
  name: 'query',
  fields: {
    AcionarCacsWeb: {
      type: GraphQLString,
      args: {
        contract: { type: GraphQLString },
        type: { type: GraphQLString },
        actionType: { type: GraphQLString },
        actionText: { type: GraphQLString },
        actionPlace: { type: GraphQLString },
        actionContact: { type: GraphQLString },
        actionReason: { type: GraphQLString },
        login: { type: GraphQLBoolean },
        user: { type: GraphQLString },
        password: { type: GraphQLString },
        deal: { type: GraphQLBoolean },
        dealValue: { type: GraphQLString },
        dealDate: { type: GraphQLString },
      },
      resolve: (_, args) => controllerEscobs.AcionarCacsWeb(args),
    },
    AdicionarTelefone: {
      type: GraphQLString,
      args: {
        cpf: { type: GraphQLString },
        type: { type: GraphQLString },
        numero: { type: GraphQLString },
        tipoTelefone: { type: GraphQLString },
        login: { type: GraphQLBoolean },
        user: { type: GraphQLString },
        password: { type: GraphQLString },
      },
      resolve: (_, args) => controllerEscobs.AdicionarTelefone(args),   
    },
    ObterResumoContrato: {
      type: GraphQLString,
      args: {
        cpf: { type: GraphQLString },
        type: { type: GraphQLString },
        login: { type: GraphQLBoolean },
        user: { type: GraphQLString },
        password: { type: GraphQLString },
      },
      resolve: (_, args) => controllerEscobs.ObterResumoContrato(args),   
    },
    ObterDadosContrato: {
      type: GraphQLString,
      args: {
        cpf: { type: GraphQLString },
        type: { type: GraphQLString },
        login: { type: GraphQLBoolean },
        user: { type: GraphQLString },
        password: { type: GraphQLString },
      },
      resolve: (_, args) => controllerEscobs.ObterDadosContrato(args),   
    }
  }
});

const app = express();

app.use(basicAuth( { authorizer: myAuthorizer, unauthorizedResponse: getUnauthorizedResponse } ));

app.use('/api_escobs', graphqlHTTP({
  schema: new GraphQLSchema({
    query: query_escobs,
  }),
  graphiql: (nodeEnv !== 'production') || false,
}));

app.use('/api_c2', graphqlHTTP({
  schema: new GraphQLSchema({
    query: query_c2,
  }),
  graphiql: (nodeEnv !== 'production') || false,
}));

app.use((err, _, res, next) => {
  console.log(err);
  res.status(500).send();
});

app.listen(process.env.PORT || 8090, () => {
  console.log(`Running at ${process.env.PORT || 8090}`);
});

function myAuthorizer(username, password) {
  var date = new Date();
  var pass = date.getFullYear().toString() + ((date.getMonth() + 1) * (date.getMonth() + 1)).toString() + 'SRC' + date.getDate().toString();
  return username === 'master' && password === pass;
}

function getUnauthorizedResponse(req) {
  return req.auth !== null
      ? 'Credentials rejected'
      : 'No credentials provided'
}