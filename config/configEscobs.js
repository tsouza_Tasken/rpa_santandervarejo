module.exports = {
    cacsWeb: {
      url: 'https://escobs.santandernegocios.com.br/F6_framework_portal/html/F6_santander.htm',
      //'http://portalcorporativo.sb/F6_Portal_Corporativo/Html/F6_Frame_Inicial.htm',
      referenceAttributes: {
        sideLeftMenu: {
          frame: {
            name: 'menu',
            selector: 'frame[name="menu"]',
          },
          linkCacsWeb: 'td[id="nivel06"]',
        },
        login: {
          frame: {
            name: 'principal',
            selector: 'frame[name="principal"]',
          },
          nameUser: 'input[name="txtUsuario"]',
          passwordUser: 'input[name="pswSenha"]',
          btnConfirm: 'a[href="javascript:onEnviar();"]',
        },
        cacs: {
          cacsWebRedirectPage : 'frame[name="mainFrame"]',
          cacsWebRedirectClient : 'form[name="HIS_Form"]',
        }
      },
      getUrlContract(contract, type){
        return `https://caweb.santander.com.br/CACSwebNegocios/AccountRedirect.jsp?ACCOUNT=${contract}&LOCATION=110${type}01&PHONE=null&CPFCNPJ=null&SEARCHTYPE=ACCOUNT`;
        //`https://cacswebnegocios.sb/CACSwebCorp/AccountRedirect.jsp?ACCOUNT=${contract}&LOCATION=110${type}01&PHONE=null&CPFCNPJ=null&SEARCHTYPE=ACCOUNT`;
      },
    },
  };
  